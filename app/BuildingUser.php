<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingUser extends Model
{
    protected $table = "building_users";
    protected $guarded = [];
}
