<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingScore extends Model
{
    protected $table = "building_score";
    protected $guarded = [];
}
