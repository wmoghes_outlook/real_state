<?php

function addClass($number)
{
    if ($number >= 0 && $number <= 33)
        return ' red ';
    elseif ($number > 33 && $number <= 66)
        return ' orange ';
    elseif ($number > 66 && $number <= 100)
        return ' green ';
}

function getAttrs($number)
{
    $arr = ["1" => "Pay on Time", "2" => "Cleanliness", "3" => "Quiet", "4" => "Friendly", "5" => "Easy to reach"];
    return $arr[$number];
}
function getAttrsIcon($number)
{
    $arr = ["1" => "payontime", "2" => "cleanliness", "3" => "quiet", "4" => "friendly", "5" => "easytoreach"];
    return $arr[$number];
}