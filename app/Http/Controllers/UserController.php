<?php

namespace App\Http\Controllers;

use App\BuildingScore;
use App\BuildingUser;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function addUser()
    {
        return view('add_user');
    }

    public function storeUser(Request $request)
    {
        $_request = $request->all();

        $user = BuildingUser::where('user_id', $request->user_id)->get();

        if (! count($user))
            BuildingUser::create([
                'user_id'   => $request->user_id,
                'name'   => $request->name,
            ]);

        $arr = [];

        $arr['area'] = $_request['area'];
        $arr['block'] = $_request['address_block'];
        $arr['street'] = $_request['address_street'];
        $arr['sub_street'] = $_request['sub_street'];
        $arr['floor'] = $_request['floor'];
        $arr['building'] = $_request['building'];
        $arr['apartment'] = $_request['apartment'];

        for ($i = 1; $i <= 5; $i++) {
           $arr['attr_'.$i] = $_request['attr_'.$i];
           $arr['desc_'.$i] = $_request['desc_'.$i];
        }

        BuildingScore::create(array_merge($arr, [
            'building_user_id' => $request->user_id,
        ]));

        return redirect()->back()
            ->withStatus("User <strong>{$request->name}</strong> has been added Successfully.");
    }

    public function getSearch()
    {
        return view('building_user_search');
    }

    public function searchUser(Request $request)
    {
        $user = BuildingUser::where('user_id', $request->search_user_id)->get();

        if (isset($user[0]) && count($user))
            $buildingScore = BuildingScore::where('building_user_id', $user[0]->user_id)->get();
        else
            return view('building_user_search')->withStatus('Not Found')->withType('danger');

        $total = [];
        $attr_1 = 0;
        $attr_2 = 0;
        $attr_3 = 0;
        $attr_4 = 0;
        $attr_5 = 0;
        if (count($buildingScore)) {
            for ($i = 0, $len = count($buildingScore); $i < $len; $i++) {
                $total[$buildingScore[$i]->id] = ((($buildingScore[$i]->attr_1 + $buildingScore[$i]->attr_2 + $buildingScore[$i]->attr_3 + $buildingScore[$i]->attr_4 + $buildingScore[$i]->attr_5) / 5) / 10) * 100;
                $attr_1 += $buildingScore[$i]->attr_1;
                $attr_2 += $buildingScore[$i]->attr_2;
                $attr_3 += $buildingScore[$i]->attr_3;
                $attr_4 += $buildingScore[$i]->attr_4;
                $attr_5 += $buildingScore[$i]->attr_5;
            }
        }

        $totalAvgOfAttr = [
            "attr_1" => round((($attr_1 / count($buildingScore)) / 10) * 100, 0),
            "attr_2" => round((($attr_2 / count($buildingScore)) / 10) * 100, 0),
            "attr_3" => round((($attr_3 / count($buildingScore)) / 10) * 100, 0),
            "attr_4" => round((($attr_4 / count($buildingScore)) / 10) * 100, 0),
            "attr_5" => round((($attr_5 / count($buildingScore)) / 10) * 100, 0)
        ];

        $buildingScore = $buildingScore->toArray();
        return view('building_user_search', compact('user', 'buildingScore', 'total', 'totalAvgOfAttr'));
    }

    public function checkUserID($id = null)
    {
        $user = BuildingUser::where('user_id', $id)->get();

        return isset($user[0]->name) ? $user[0]->name : null;
    }
}
