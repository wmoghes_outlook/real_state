-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: real_state
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `building_score`
--

DROP TABLE IF EXISTS `building_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_1` varchar(50) DEFAULT NULL,
  `attr_2` varchar(50) DEFAULT NULL,
  `attr_3` varchar(50) DEFAULT NULL,
  `attr_4` varchar(50) DEFAULT NULL,
  `attr_5` varchar(50) DEFAULT NULL,
  `desc_5` varchar(200) DEFAULT NULL,
  `building_user_id` varchar(150) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desc_4` varchar(200) DEFAULT NULL,
  `desc_3` varchar(200) DEFAULT NULL,
  `desc_2` varchar(200) DEFAULT NULL,
  `desc_1` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `building` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `apartment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `building_score`
--

LOCK TABLES `building_score` WRITE;
/*!40000 ALTER TABLE `building_score` DISABLE KEYS */;
INSERT INTO `building_score` VALUES (1,'7','9','9','10','5','Very Good','123','2019-01-04 15:14:43','2019-01-04 15:14:43',NULL,'Anything',NULL,NULL,'Cairo','5','4','4'),(2,'1','0','0','2','0','Very bad','123','2019-01-04 15:20:31','2019-01-04 15:20:31',NULL,NULL,'Bad','Bad','Giza','96','52','4'),(3,'4','7','9','9','9',NULL,'123','2019-01-04 15:26:28','2019-01-04 15:26:28',NULL,NULL,NULL,'Hmm, No thing important to say :(','Maadi','96','6','5'),(4,'10','10','10','10','9',NULL,'123','2019-01-04 15:35:08','2019-01-04 15:35:08',NULL,NULL,NULL,NULL,'Nile City','96','6','6'),(5,'5','6','8','9','10','Good','456','2019-01-04 15:35:51','2019-01-04 15:35:51',NULL,NULL,NULL,NULL,'New cairo','5','6','6'),(6,'7','8','8','10','10',NULL,'999','2019-02-09 11:06:36','2019-02-09 11:06:36','Something',NULL,'Very Nice',NULL,'Cairo','66','44','4'),(7,'3','7','1','9','10',NULL,'1','2019-02-15 04:43:40','2019-02-15 04:43:40',NULL,'very noisy kids',NULL,NULL,'sulabikhat','33','1','2'),(8,'8','7','8','9','10',NULL,'12312','2019-02-19 16:04:03','2019-02-19 16:04:03',NULL,NULL,NULL,NULL,'test','3','2','2');
/*!40000 ALTER TABLE `building_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `building_users`
--

DROP TABLE IF EXISTS `building_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(200) DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `building_users`
--

LOCK TABLES `building_users` WRITE;
/*!40000 ALTER TABLE `building_users` DISABLE KEYS */;
INSERT INTO `building_users` VALUES (1,'123','Waleed Ali','2019-01-04 15:14:43','2019-01-04 15:14:43'),(2,'456','Mohamed Ahmed','2019-01-04 15:35:51','2019-01-04 15:35:51'),(3,'999','Mohamed Ali','2019-02-09 11:06:36','2019-02-09 11:06:36'),(4,'1','sarah shaker','2019-02-15 04:43:40','2019-02-15 04:43:40'),(5,'12312','test','2019-02-19 16:04:03','2019-02-19 16:04:03');
/*!40000 ALTER TABLE `building_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `is_admin` int(11) DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Waleed','wmoghes@gmail.com',NULL,0,'$2y$10$xljlOPScGbJ5.d7/x3auHOP9cnU4pib7LNlXQ8Lpz4RINVVXb1eZm','XjlIuxGdHdqGt2TPslx1jUrQrn90rwuiBVRLn1MLQroPDtIlqqgQ4AnarhwC','2018-12-24 09:22:52','2018-12-24 09:22:52'),(2,'admin','admin@admin.com',NULL,1,'$2y$10$m4tlaDP5xrcDENzy7WaKheRz1S5nxN2K8QUDgY4p83SHm0JtLtZk6','kt1ofHkiWMudQMpDQZzcMbGZ8OrsQfEKS7yJS2fUx5KfPNWvwvUX8UgHh9hd','2018-12-24 18:37:24','2018-12-24 18:37:24'),(3,'user','user@user.com',NULL,0,'$2y$10$GKGKbwxd.LZjcUSeVZ3cKeXnLmokrBGuj4K9ZaRyOaXDMetlgEb5q','wzgpzYU4uD35TI08qw85Kj4t3haHWStfWBOkmkIhHLGI4xY6ePg6pX35x5zx','2018-12-24 18:38:23','2018-12-24 18:38:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-19 17:23:27
