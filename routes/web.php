<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/add-user', 'UserController@addUser')->name('add_user');
    Route::post('/add-user', 'UserController@storeUser')->name('store_user');

});

Route::group(['middleware' => ['auth']], function () {

    Route::get('/get-search', 'UserController@getSearch')->name('get_search');
    Route::get('/search-building-user', 'UserController@searchUser')->name('search_building_user');

    Route::get('/check-user-id/{id?}', 'UserController@checkUserID')->name('check_user_id');

    Route::get('user-logout', 'HomeController@logout')->name('user_logout');
});
