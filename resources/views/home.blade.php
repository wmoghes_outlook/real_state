@extends('layouts.app')
@push('body-class', 'dashboard')
@push('header_Block', 'header_Block')


@section('ContentHeader')
<img src="{{ asset('images/curve-hide.png') }}" alt="" class="sectionCurve">
<div class="centerContent container">

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            @if (Auth::user()->is_admin == 1)
                <div class="col-md-12">
                        <sub>Overview / Admin Dashboard</sub>
                        <h1>Adding Tenants</h1>
                        <p class="lead">
                            Here you can start adding tenant
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum dolores excepturi possimus accusamus odio optio, expedita similique, quasi eius magnam eveniet, necessitatibus suscipit ab corporis dolor minus ipsa, blanditiis dignissimos!
                        </p>
                        <a href="{{ route('add_user') }}" class="button-custom green mt-10">Add a Tenant</a>
                </div>
            @else
                @include('includes.shareduser')
            @endif
        </div>

</div>
@endsection
