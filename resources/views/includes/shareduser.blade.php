<div class="col-md-12">
    <div class="">
        <h1>Search for a Tenant</h1>
        <p class="lead">
            Here you can search for Tenants
            <br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum dolores excepturi possimus accusamus odio optio, expedita similique, quasi eius magnam eveniet, necessitatibus suscipit ab corporis dolor minus ipsa, blanditiis dignissimos!
        </p>
        <!-- <a href="{{ route('get_search') }}" class="button-custom green mt-10">Search for Tenant</a> -->


        <form class="col-12" action="{{ route('search_building_user') }}" method="get">
            <div class="row">
                <div class="col-12 col-md-7 searchUser">
                    <input id="js-search-user-id" type="text" placeholder="Search using ID#"
                       name="search_user_id" value="{{ old('search_user_id') }}"
                       required autofocus>
                    <button type="submit" class="button-custom green">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>

