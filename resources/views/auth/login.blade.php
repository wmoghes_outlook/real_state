@extends('layouts.app')
<!-- @push('styles')
    <link href="{{ asset('css/app22.css') }}" />
    @endpush -->

@push('body-class', 'login')

@section('content')
    <div class="slideBox">
        <div class="content">
            <div id="cd-logo"><a href="#0"><img src="images/cd-logo_1.svg" alt="Logo"></a></div>
            <h2>Login</h2>
            <form id="form-login" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-element form-stack">
                    <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-element form-stack">
                    <label for="password"  class="form-label">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-element form-check form-stack">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
                <div class="form-element form-submit">
                    <button class="green button-custom" type="submit" name="login">Log In</button>
                </div>

            </form>
        </div>
    </div>
@endsection
