@extends('layouts.app')
@push('body-class', 'dashboard')
@push('header_Block', 'header_Block')


@section('ContentHeader')
    <img src="{{ asset('images/curve-hide.png') }}" alt="" class="sectionCurve">
    <div class="centerContent container">
        <div class="row">
            @include('includes.shareduser')
        </div>
    </div>
@endsection

@section('content')
<div class="container">
    @if (isset($user) && count($user) && isset($buildingScore))

    @push('header_BlockResults', 'header_Block--short')

    @for ($i = 0; $i < count($user); $i++)
    <div class="userPIC row">
        <div class="col text-center">
            <div class="c100 center p{{ round((array_sum($total) / count($total)), 0) }} big {{ addClass((array_sum($total) / count($total))) }}">
              <span>
                <img class="img-circle" src="{{ url('images/ppic.png') }}" />
              </span>
              <div class="slice">
                <div class="bar"></div>
                <div class="fill"></div>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4 class="col-12 m-t-0 m-b-5 text-center header-title"><b>{{ $user[$i]->name }} ({{ (array_sum($total) / count($total)) }}%)</b></h4>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="avg-Atributes-container">
                @for ($y = 1; $y <= 5; $y++)
                    <div class="avgAtts">
                        <div class="c100 p{{ $totalAvgOfAttr['attr_' . $y] }} 
                                {{ addClass($totalAvgOfAttr['attr_' . $y]) }}">
                            <i class="icon-{{ getAttrsIcon($y) }}"></i>
                          <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                          </div>
                        </div>
                        <br>
                        <b>{{ getAttrs($y) }}</b><br>{{ $totalAvgOfAttr['attr_' . $y] }}%
                    </div>
                @endfor
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 my-2">
            <h3>Personal Information</h3>
            <p>
                <b>Age</b>: 24.
                <br>
                <b>Nationality</b>: Egyptian.
                <br>
                <b>Gender</b>: Male.
                <br>
                <b>Marital Status</b>: Married.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <hr>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-12 my-2">
            <h3>Previous rent</h3>
        </div>
        <div class="col-6">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d55671.47841836068!2d47.91457880032596!3d29.29796223881774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1smarket!5e0!3m2!1sen!2seg!4v1547717608332" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-12" id="accordion">
                    @for ($z = 0; $z < count($buildingScore); $z++)
                    <div class="border m-top--1">
                        <div class="row">

                            <!-- Title -->
                            <div class="col-12">
                                <div class="historyHead collapsed d-flex" data-toggle="collapse" data-target="#{{'collapse' . $z}}" aria-expanded="false" aria-controls="{{'collapse' . $z}}">
                                    <div>
                                       <b>Area</b>: {{ $buildingScore[$z]['area'] }},
                                       <b>Block</b>: {{ $buildingScore[$z]['block'] }},
                                       <br>

                                       <b>Street</b>: {{ $buildingScore[$z]['street'] }},
                                       <b>Sub Street</b>: {{ $buildingScore[$z]['sub_street'] }},
                                       <br>
                                        
                                       <b>Building</b>: {{ $buildingScore[$z]['building'] }} , 
                                       <b>Floor</b>: {{$buildingScore[$z]['floor'] }} ,
                                       <b>Apt</b>: 

                                       {{ $buildingScore[$z]['apartment'] }}.
                                   </div>
                                    <div class="progressTOP">
                                        <div class="progress-bar" role="progressbar"
                                            style="width: {{ $total[$buildingScore[$z]['id']] }}%;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                {{ $total[$buildingScore[$z]['id']] }}%
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <!-- Content -->
                            <div class="col-12">
                                <div id="{{'collapse' . $z}}" class="collapseContent collapse" data-parent="#accordion">
                                    @for ($y = 1; $y <= 5; $y++)
                                        <div class="singleHistory">
                                            <div class="singleHistory__Icon">
                                                <i class="icon-{{ getAttrsIcon($y) }}"></i>
                                            </div>
                                            <div class="singleHistory__content">
                                                <div class="col-12">
                                                    <b>{{ getAttrs($y) }}</b>: {{ $buildingScore[$z]['attr_'.$y] }}0%
                                                </div>
                                                @if (! empty($buildingScore[$z]['desc_'.$y]) && ! is_null($buildingScore[$z]['desc_'.$y]))
                                                    <div class="col-12 description">
                                                        {{ $buildingScore[$z]['desc_'.$y] }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor

                </div>
            </div>
        </div>
    </div>

    @endfor

   
    @endif
</div>
@endsection
