@extends('layouts.app')
@push('body-class', 'dashboard')
@push('nav-class', 'main-navbar')


@section('content')

<div class="container">
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle mb-2">OVERVIEW / Admin Dashboard</span>
        <h3 class="page-title">
            <img src="images/ppic.png" class="titlePIC" alt="">
            Add User
        </h3>
      </div>
    </div>
    @if (session('status'))
        <div class="row">
            <div class="col-12 d-flex justify-content-center align-content-center successBox">
                <img src="images/check.svg" alt="">
                <p>{!! session('status') !!}</p>
                <div class="buttons">
                    <a class="button-custom green border-radius-EXTRA" href="{{ route('add_user') }}">add more</a>
                    <a class="button-custom btn-dark border-radius-EXTRA" href="{{ url('/home') }}">Home</a>
                </div>
            </div>
        </div>
    @else
        <div class="row mb-4">
            <form class="col" action="{{ route('store_user') }}" method="post">
                {{ @csrf_field() }}
                <div class="row">
                    <div class="col-2 col-lg-2">
                        <label class="col-form-label text-primary">Account Details</label>
                    </div>
                    <div class="col-10 col-lg-10">
                        <div class="form-group row">
                            <label for="js-user-id" class="col-sm-3 col-form-label text-right">Tenant civil ID</label>
                            <div class="col-sm-9">
                                <input pattern=".{12,12}" type="number" 
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                    minlength="12" maxlength="12"
                                id="js-user-id" class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id" onblur="checkID(this.value)" value="{{ old('user_id') }}" required title="12 characters neededs">
                                <span id="js-status" style="display: none">Checking....</span>
                                @if ($errors->has('user_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="js-name" class="col-sm-3 col-form-label text-right">Full Name:</label>
                            <div class="col-sm-9">
                                <input id="js-name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="js-address" class="col-sm-3 col-form-label text-right">Address:</label>
                            <div class="col-sm-9">

                                <div class="form-group row">
                                    <div class="col-6">
                                        <input id="js-address" type="text" class="form-control{{ $errors->has('area') ? ' is-invalid' : '' }}"
                                        name="area" placeholder="Area" value="{{ old('area') }}" required>
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control{{ $errors->has('address_block') ? ' is-invalid' : '' }}"
                                        name="address_block" placeholder="Block" value="{{ old('address_block') }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <input type="text" class="form-control{{ $errors->has('address_street') ? ' is-invalid' : '' }}"
                                        name="address_street" placeholder="Street" value="{{ old('address_street') }}" required>
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control{{ $errors->has('sub_street') ? ' is-invalid' : '' }}"
                                        name="sub_street" placeholder="Sub Street" value="{{ old('sub_street') }}" required>
                                    </div>
                                </div>




                                <div class="form-group row">
                                    <div class="col-4">
                                        <input type="text" class="form-control{{ $errors->has('building') ? ' is-invalid' : '' }}"
                                        name="building" placeholder="Building" value="{{ old('building') }}" required>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control{{ $errors->has('floor') ? ' is-invalid' : '' }}"
                                        name="floor" placeholder="Floor" value="{{ old('floor') }}" required>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control{{ $errors->has('apartment') ? ' is-invalid' : '' }}"
                                        name="apartment" placeholder="Apartment No" value="{{ old('apartment') }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col"><hr></div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <label class="col-form-label text-primary">Add Score</label>
                    </div>
                    <div class="col-10">
                        @for($i = 1; $i <= 5; $i++)
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">{{ getAttrs($i) }} <i class="icon__mod icon-{{ getAttrsIcon($i) }}"></i></label>
                            <div class="col-sm-9">
                                <div class="form-group row">
                                    <div class="col-12">
                                       <input
                                        class="col-12"
                                        name="attr_{{ $i }}"
                                        id="range-slider-input{{ $i }}"
                                        type="range"
                                        role="slider"
                                        aria-valuemin="0"
                                        aria-valuemax="10"
                                        aria-valuetext="2"
                                        min="0"
                                        max="10"
                                        step="1"
                                        value="{{ $i + 5 }}"
                                        values='{
                                        "0": "0",
                                        "1": "1",
                                        "2": "2",
                                        "3": "3",
                                        "4": "4",
                                        "5": "5",
                                        "6": "6",
                                        "7": "7",
                                        "8": "8",
                                        "9": "9",
                                        "10": "10"
                                        }'/> 
                                    </div>
                                    
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="custom-control custom-checkbox">
                                          <input class="custom-control-input" type="checkbox" id="js-add-desc-{{ $i }}" onclick="addDescription(this.checked, {{ $i }})">
                                          <label class="custom-control-label" for="js-add-desc-{{ $i }}">Add Description</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12" id="js-desc-{{ $i }}" style="display: none;">
                                        <textarea name="desc_{{ $i }}" id="js-textarea-val-{{ $i }}" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="row"><div class="col"><hr></div></div>
                <div class="row ">
                    <div class="d-flex col-12 justify-content-end">
                        <button type="submit" class="button-custom green">Save User</button>
                    </div>
                </div>
            </form>
        </div>
    @endif
</div>
@endsection


@section('script')
    <script>
        function checkID(_val) {
            var url = '{{ route('check_user_id') }}' + '/' + _val;
            document.getElementById('js-status').style.display = "block";

            $.ajax({
                type: 'get',
                url: url,
                success: function (response) {
                    if (response) {
                        document.getElementById('js-name').value = response;
                        document.getElementById('js-name').readOnly = true;
                    }

                    document.getElementById('js-status').style.display = "none";
                }
            })
        }

        function addDescription(_val, id) {
            if (_val)
                document.getElementById('js-desc-' + id).style.display = "block";
            else {
                document.getElementById('js-textarea-val-' + id).value = "";
                document.getElementById('js-desc-' + id).style.display = "none";
            }

        }
    </script>
@endsection

@push('scripts')
<script src="{{ asset('js/custom.js') }}"></script>
@endpush