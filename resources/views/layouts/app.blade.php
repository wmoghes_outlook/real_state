<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- @stack('styles') -->
</head>
<body class="@stack('body-class')">
    <div class="wrapper">
        @guest
        @else
        <header class="@stack('header_Block') @stack('header_BlockResults') container-fluid">
            <div class="row">
                <nav class="col navbar @stack('nav-class') sticky-top navbar-expand-md navbar-dark mb-4">
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('/home') }}"><img src="images/cd-logo_1.svg" class="bg-primary" alt="Logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest

                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            @if (Auth::user()->is_admin == 1)
                                                <a class="dropdown-item" href="{{ route('add_user') }}">
                                                    <i class="material-icons">playlist_add</i> Add User
                                                </a>
                                            @else
                                                <a class="dropdown-item" href="{{ route('get_search') }}">
                                                    Get Search
                                                </a>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item text-danger" href="{{ route('user_logout') }}">
                                                <i class="material-icons text-danger">&#xE879;</i> {{ __('Logout') }}
                                            </a>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            
        @endguest

        @yield('ContentHeader')
        </header>
        @yield('content')

        
        @guest
        @else
        <div class="container-fluid">
            <footer class="row">
                <div class="col-12 foot py-4 d-flex justify-content-center">
                    <img class="mb-4" src="images/cd-logo_1-footer.svg" alt="">
                    <p>Copy rights by RealState iScore (C) 2019</p>
                    <p>All rights reserved</p>
                </div>
            </footer>
        </div>
        @endguest


    </div>


    @yield('script')
    @stack('scripts')
</body>
</html>
